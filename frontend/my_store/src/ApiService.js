import axios from "axios";
import EventBuss from './common/EventBus';

export default class ApiService {

    static async login(email, password) {
        const userData = {
            email: email,
            password: password,
        };
        try {
            const response = await axios.post("http://localhost:3000/login", userData);
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            }
            else {
                return response.data;
            }
        } catch (e) {
            throw e;
        }
    }

    static async signup(username, email, password, confirmPassowrd) {
        const newUserData = {
            username: username,
            email: email,
            password: password,
            confirmPassword: confirmPassowrd,
        };
        try {
            const response = await axios.post("http://localhost:3000/signup", newUserData);
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            }
            else {
                return response.data.token;
            }
        }
        catch (e) {
            throw e;
        }
    }

    static async getCart() {
        try {
            const response = await axios.get("http://localhost:3000/cart", { headers: { authorization: localStorage.getItem("AuthToken") } });
            console.log(response.data);
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            } else {
                return [response.data.products, response.data.totalPrice];
            }

        } catch (e) {
            throw e;
        }
    }

    static async addItemToCart(id) {
        const body = {
            amount: 1,
            id: id
        }
        try {
            const response = await axios.post("http://localhost:3000/cart/products", body, { headers: { authorization: localStorage.getItem("AuthToken") } });
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            }
        } catch (e) {
            throw e;
        }
    }

    static async removeItemFromCart(id) {
        try {
            const response = await axios.delete(`http://localhost:3000/cart/products/${id}`, { headers: { authorization: localStorage.getItem("AuthToken") } });
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            }
        }
        catch (e) {
            throw e;
        }
    }

    static async addProduct(name, description, picture, price, quantity) {
        const body = {
            product: {
                name: name,
                description: description,
                picture: picture,
                price: price,
                quantity: quantity
            }
        };
        try {
            const response = await axios.post("http://localhost:3000/products", body, { headers: { authorization: localStorage.getItem("AuthToken") } });
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            }
        } catch (e) {
            throw e;
        }
    }

    static async getUsername() {
        try {
            const response = await axios.get("http://localhost:3000/user", { headers: { authorization: localStorage.getItem("AuthToken") } });
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            } else {
                console.log(response);
                return response.data.userCredentials.username.toLowerCase();
            }

        } catch (e) {
            //throw e;
            console.log(e);
        }
    }

    static async deleteProduct(id) {
        try {
            const response = await axios.delete(`http://localhost:3000/products/${id}`, { headers: { authorization: localStorage.getItem("AuthToken") } });
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            }
        }
        catch (e) {
            throw e;
        }
    }

    static async getProducts() {
        console.log("Token = " + localStorage.getItem("AuthToken"));
        try {
            const response = await axios.get("http://localhost:3000/products", { headers: { authorization: localStorage.getItem("AuthToken") } });
            if (response.status === 299) {
                EventBuss.dispatch('logout');
            } else {
                return response.data;
            }
        } catch (e) {
            throw e;
        }
    }
}