import React, { useEffect } from "react";
import CartItem from "./CartItem";
import { useState } from "react";
import { Box, Flex, Grid, GridItem, HStack, Spacer, Text } from "@chakra-ui/layout";
import { Modal, Button, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter } from "@chakra-ui/react";
import ApiService from "../ApiService";

const Cart = (props) => {
    const [cartItems, setCartItems] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);

    useEffect(() => {
        const getCart = async () => {
            try {
                const [products, totPrice] = await ApiService.getCart();
                setCartItems(products);
                setTotalPrice(totPrice);

            } catch {
                console.log("Error while getting cart");
            }
        };
        if (props.isOpen)
            getCart();
    }, [props.isOpen])


    const orderHandler = () => {

    };


    return (
        <Modal isOpen={props.isOpen} onClose={props.onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader alignSelf="center">My Cart</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Flex>
                        <Grid templateColumns="repeat(2, 1fr)" gap={4} w="100%">
                            {cartItems.map((item, index) => {
                                return <GridItem colSpan={1}> <CartItem key={index} name={item.name} price={item.price} amount={item.amount} id={item.id} /></GridItem>
                            })}
                        </Grid>
                    </Flex>
                </ModalBody>
                <ModalFooter>
                    <HStack w="100%">
                        <Text justifySelf="left" fontWeight="bold">Total Price: {totalPrice}$</Text>
                        <Spacer />
                        <Box justifySelf="right" flexDirection="raw">
                            <Button onClick={orderHandler} borderRadius={10} backgroundColor="#D6D2C4">Order</Button>
                            <Button onClick={props.onClose} borderRadius={10} ml="3" >Close</Button>
                        </Box>
                    </HStack>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
};

export default Cart;