import React, { Fragment, useState } from "react";
import { Button } from "@chakra-ui/button";
import { Flex, VStack, Text, HStack } from "@chakra-ui/layout";
import ApiService from "../ApiService";

const CartItem = (props) => {
    const [amount, setAmount] = useState(props.amount);

    const addItemHandler = async () => {
        try {
            await ApiService.addItemToCart(props.id);
            setAmount(amount + 1);
        } catch (e) {
            console.log("error while adding item " + e);
        }
    };

    const removeItemHadler = async () => {
        try {
            await ApiService.removeItemFromCart(props.id);
            setAmount(amount - 1);
        }
        catch { }
    }

    return (
        <Fragment>
            {amount > 0 &&
                <VStack flexDirection="column" alignItems="center" w="100%">
                    <Flex flexDirection="column" alignItems="center">
                        <Text fontSize="large">{props.name}</Text>
                        <Flex flexDirection="column" alignItems="center">
                            <Text fontWeight="bold">price: {props.price}$</Text>
                            <Text fontWeight="bold" borderRadius="6">x {amount}</Text>
                        </Flex>
                    </Flex>
                    <Flex flexDirection="raw" >
                        <HStack>
                            <Button borderRadius={5} onClick={addItemHandler}>+</Button>
                            <Button borderRadius={5} onClick={removeItemHadler}>-</Button>
                        </HStack>
                    </Flex>
                </VStack>
            }
        </Fragment>
    );
};

export default CartItem;