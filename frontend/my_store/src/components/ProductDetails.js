import React from "react";
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalCloseButton, ModalFooter } from "@chakra-ui/modal";
import { HStack, VStack, Text } from "@chakra-ui/layout";
import { Button } from "@chakra-ui/button";
import { Image } from "@chakra-ui/image";

const ProductDetails = (props) => {

    return (
        <Modal isOpen={props.isOpen} onClose={props.onClose}>
            <ModalOverlay />
            <ModalContent>

                <ModalHeader alignSelf="center">{props.product.name}</ModalHeader>
                <ModalCloseButton />
                <ModalBody >

                    <VStack >
                        <Image src={props.product.picUrl} objectFit="cover" />
                        <Text alignSelf="flex-start">Owner: {props.product.owner}</Text>
                        <Text alignSelf="flex-start">Price: {props.product.price}$</Text>
                        <Text isTruncated width="100%" textOverflow="ellipsis">Description: {props.product.description}</Text>
                    </VStack>
                </ModalBody>
                <ModalFooter>
                    <HStack>
                        <Button onClick={props.onClose} borderRadius={10} >Close</Button>
                    </HStack>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
};

export default ProductDetails;