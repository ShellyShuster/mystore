import React from "react";
import { Flex } from "@chakra-ui/react";
import { useState } from "react";
import { Button } from "@chakra-ui/button";
import { Input } from "@chakra-ui/input";
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalCloseButton, ModalFooter } from "@chakra-ui/modal";
import ApiService from "../ApiService";

const NewProduct = (props) => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState('');
    const [picture, setPicture] = useState("");
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);

    const changeHandler = (event) => {
        if (event.target.name === "name") {
            setName(event.target.value);
        } else if (event.target.name === "description") {
            setDescription(event.target.value);
        } else if (event.target.name === "picture") {
            setPicture(event.target.value);
        } else if (event.target.name === "price") {
            setPrice(event.target.value);
        } else if (event.target.name === "quantity") {
            setQuantity(event.target.value);
        }
    };

    const submitHandler = async () => {
        try {
            await ApiService.addProduct(name, description, picture, price, quantity);
            props.onClose();
        } catch (e) {
            console.log("error while adding a product " + e);
        }
        props.onUpdateProducts();
    };

    return (
        <Modal isOpen={props.isOpen} onClose={props.onClose} >
            <ModalOverlay />
            <ModalContent backgroundColor="#faf8ea">

                <ModalHeader alignSelf="center" textAlign="center" color="#7D5A50">New Product</ModalHeader>
                <ModalCloseButton />
                <ModalBody >

                    <Flex alignItems="center" flexDirection="column" >
                        <Input w="85%" color="#7D5A50" placeholder="name" variant="filled" mb={6} _placeholder={{ color: "white" }} type="text" background="#e2bf9c" name="name" id="name" onChange={changeHandler} />
                        <Input w="85%" color="#7D5A50" placeholder="description" variant="filled" mb={6} _placeholder={{ color: "white" }} type="text" background="#e2bf9c" name="description" id="description" onChange={changeHandler} />
                        <Input w="85%" color="#7D5A50" placeholder="picture address" variant="filled" mb={6} _placeholder={{ color: "white" }} background="#e2bf9c" name="picture" id="picture" onChange={changeHandler} />
                        <Input w="85%" color="#7D5A50" placeholder="price" variant="filled" mb={6} _placeholder={{ color: "white" }} type="number" background="#e2bf9c" name="price" id="price" onChange={changeHandler} />
                        <Input w="85%" color="#7D5A50" placeholder="quantity" variant="filled" mb={6} _placeholder={{ color: "white" }} type="number" background="#e2bf9c" name="quantity" id="quantity" onChange={changeHandler} />
                    </Flex>
                </ModalBody>
                <ModalFooter alignSelf="center">
                    <Button borderRadius={10} background="#e2bf9c" color="#7D5A50" onClick={submitHandler}>Add Product</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
};

export default NewProduct;
