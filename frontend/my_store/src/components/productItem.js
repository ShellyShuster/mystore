import React, { useEffect, useState } from "react";
import { Box, GridItem, StackDivider, VStack, Grid, Flex, Spacer } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import ProductDetails from "./ProductDetails";
import ApiService from "../ApiService";

const ProductItem = (props) => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [isOwner, setIsOwner] = useState(false);

    useEffect(() => {
        const getUsername = async () => {
            try {
                const username = await ApiService.getUsername();
                console.log("username = " + username);
                setIsOwner(username === props.product.owner.toLowerCase());

            } catch (e) {
                console.log("error while getting products " + e);
            }
        }
        getUsername();

    }, []);

    const addToCartHandler = async () => {
        try {
            await ApiService.addItemToCart(props.product.productId);
        } catch (e) {
            console.log("Error while adding to cart " + e);
        }
    };


    const deleteProductHandler = async () => {
        props.onDeleteProduct(props.product.productId);
    };

    return (
        <Grid templateColumns="repeat(5, 1fr)" gap={4} h="200px" w="500px">
            <GridItem colSpan={2}>
                <Image objectFit="contain" src={props.product.picUrl} alt={props.product.name} h="200px" />
            </GridItem>
            <GridItem colSpan={3}>
                <Flex flexDirection="row" bg=" #eed9c4" borderRadius={15}>
                    <VStack

                        divider={<StackDivider borderColor="gray.200" />}
                        spacing={3}
                        align="stretch"
                    >
                        <Flex m={5} flexDirection="column" h='150px'>
                            <div onClick={onOpen}>
                                <Box fontSize="3xl">{props.product.name}</Box>
                                <Box>Owner: {props.product.owner}</Box>
                                <Box>Price: {props.product.price}$</Box>
                            </div>
                            <Button w="fit-content" mt={3} borderRadius={10} backgroundColor="#B4846C" onClick={addToCartHandler}>Add To Cart</Button>
                            <ProductDetails onClose={onClose} isOpen={isOpen} product={props.product} />
                        </Flex>
                    </VStack>
                    <Spacer />
                    {isOwner && <Button w="fit-content" mr={5} mt={5} borderRadius={10} backgroundColor="#B4846C" onClick={deleteProductHandler}>-</Button>}
                </Flex>
            </GridItem>
        </Grid>

    );
};

export default ProductItem;