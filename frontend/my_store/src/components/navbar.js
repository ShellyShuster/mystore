import React, { useState } from "react";
import { Flex, HStack, Spacer, Text } from "@chakra-ui/layout";
import { useHistory } from "react-router";
import Cart from "./cart";
import { useDisclosure } from "@chakra-ui/hooks";
import { Button } from "@chakra-ui/react";
import NewProduct from "./NewProduct";

const Navbar = (props) => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const history = useHistory();
    const [isNewProduct, setIsNewProduct] = useState(false);

    const logoutHandler = () => {
        localStorage.removeItem('AuthToken');
        history.push('/login');
    }

    const newProductHandler = () => {
        setIsNewProduct(true)
        onOpen();
    }

    const onCloseNewProduct = () => {
        setIsNewProduct(false);
        onClose();
    }


    return (
        <Flex as="header" backgroundColor="#B4846C" backdropFilter="saturate(180%) blur(5px)" w="100%" h="50px">
            <HStack w="100%">
                <Text pl={5} fontFamily="fantasy" color="#eed9c4" fontSize="3xl" fontWeight="semibold">My Store</Text>
                <Spacer />
                <Flex pr="5">
                    <HStack>
                        <Button onClick={onOpen} backgroundColor="#eed9c4">
                            <span>
                                <img alt="cart" src="https://img.icons8.com/external-kiranshastry-lineal-kiranshastry/20/000000/external-shopping-cart-interface-kiranshastry-lineal-kiranshastry-1.png" />
                            </span>
                            <Text padding="2">Cart</Text>
                        </Button>
                        <Button onClick={newProductHandler} backgroundColor="#eed9c4">New Product</Button>
                        <Button onClick={logoutHandler} backgroundColor="#eed9c4">Logout</Button>
                        {isNewProduct ? (
                            <NewProduct onClose={onCloseNewProduct} isOpen={isOpen} onUpdateProducts={props.OnUpdateProducts} />
                        ) : (
                            <Cart onClose={onClose} isOpen={isOpen} />
                        )}

                    </HStack>
                </Flex>
            </HStack>
        </Flex >
    );
};

export default Navbar;