import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./pages/Login";
import ProductsList from "./pages/productsList";
import Signup from "./pages/Signup";
import { useHistory } from "react-router";

import EventBus from "./common/EventBus";

function App() {
  const history = useHistory();

  const logout = () => {
    localStorage.removeItem("AuthToken");
    localStorage.removeItem("username");
    //history.push('/login');
    document.location.href = "/login"
  }

  useEffect(() => {
    EventBus.on("logout", () => {
      logout();
    })
  }, []);

  return (
    <div>
      <Router>
        <div>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/" component={ProductsList} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}
export default App;
