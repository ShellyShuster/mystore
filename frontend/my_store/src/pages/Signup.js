import { Button, Flex, Heading, Input } from '@chakra-ui/react'
import React from 'react';
import { useState } from "react";
import { useHistory } from "react-router";
import ApiService from '../ApiService';

const Signup = () => {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassowrd, setConfirmPassword] = useState();

  const history = useHistory();

  const changeHandler = (event) => {
    if (event.target.name === "username") {
      setUsername(event.target.value);
    } else if (event.target.name === "password") {
      setPassword(event.target.value);
    } else if (event.target.name === "confirmPassword") {
      setConfirmPassword(event.target.value);
    } else if (event.target.name === "email") {
      setEmail(event.target.value);
    }
  };

  const submitHandler = async (event) => {
    event.preventDefault();
    try {
      const token = await ApiService.signup(username, email, password, confirmPassowrd);
      localStorage.setItem("AuthToken", `${token}`);
      history.push("/");
    } catch (error) {
      console.log("error in signup");
    }
  };
  return (
    <Flex height='100vh' alignItems="center" justifyContent="center" background="#faf0e6">
      <Flex height='400px' width='400px' direction="column" background="#eed9c4" p={12} borderRadius={10}>
        <Heading mb={6} textAlign="center" color="#7D5A50">Sign Up</Heading>
        <Input color="#7D5A50" _placeholder={{ color: 'White' }} placeholder="username" variant="filled" mb={3} background="#B4846C" name="username" id="username" onChange={changeHandler} />
        <Input color="#7D5A50" _placeholder={{ color: 'White' }} placeholder="email" variant="filled" mb={3} background="#B4846C" name="email" id="email" onChange={changeHandler} />
        <Input color="#7D5A50" _placeholder={{ color: 'White' }} placeholder="password" variant="filled" mb={3} type="password" background="#B4846C" name="password" id="password" onChange={changeHandler} />
        <Input color="#7D5A50" _placeholder={{ color: 'White' }} placeholder="confirm password" variant="filled" mb={3} type="password" background="#B4846C" name="confirmPassword" id="confirmPassword" onChange={changeHandler} />
        <Button mt={6} alignSelf="center" width="50%" background="#faf8ea" color="#7D5A50" disabled={!username || !password} onClick={submitHandler}>Sign Up</Button>
      </Flex>
    </Flex>
  );
};


export default Signup;
