import React, { useEffect, useState } from "react";
import ProductItem from "../components/productItem";
import { Flex } from "@chakra-ui/layout";
import Navbar from "../components/navbar";
import { useHistory } from "react-router";
import ApiService from "../ApiService";

const ProductsList = () => {
    const [products, setProducts] = useState([]);
    const history = useHistory();

    useEffect(() => {
        let subscribed = true;
        if (!localStorage.getItem("AuthToken") || localStorage.getItem("AuthToken").length === 0) {
            history.push('/login');
        }
        const getProducts = async () => {
            try {
                const pros = await ApiService.getProducts();
                if (subscribed) {
                    setProducts(pros);
                }
            } catch (e) {
                console.log(e);
            }

        }
        getProducts();

        return () => {
            subscribed = false;
        }
    }, []);

    const deleteProduct = async (id) => {
        await ApiService.deleteProduct(id);
        updateProducts();
    };

    const updateProducts = async () => {
        try {
            const pros = await ApiService.getProducts();


            setProducts(pros);
        } catch (e) {
            console.log("error while getting products " + e);
        }
    }


    return (
        <>
            <Navbar OnUpdateProducts={updateProducts} />
            <Flex alignItems="center" justifyContent="center" flexDirection="column" >
                <Flex flexDirection="column" h="100vh" mt={5}>
                    {products.length > 0 ? (
                        products.map((product, index) => {
                            return <ProductItem key={index} product={product} onDeleteProduct={deleteProduct} />
                        })
                    ) : (
                        <p>No pruducts found!</p>
                    )}
                </Flex>
            </Flex>

        </>
    );
};

export default ProductsList;