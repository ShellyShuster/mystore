import { Button, Flex, Heading, Input, Text } from '@chakra-ui/react'
import React from 'react';
import { useState } from "react";
import { useHistory } from "react-router";
import { Box, Spacer } from '@chakra-ui/layout';
import ApiService from '../ApiService';

const Login = () => {

  const [email, setemail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(" ")
  const history = useHistory();

  const changeHandler = (event) => {
    if (event.target.name === "email") {
      setemail(event.target.value);
    } else if (event.target.name === "password") {
      setPassword(event.target.value);
    }
  };


  const submitHandler = async (event) => {
    event.preventDefault();
    try {
      const data = await ApiService.login(email, password)
      console.log(data);
      if (data.error) {
        setError(data.error);
      } else {
        setError("");
        localStorage.setItem("AuthToken", `Bearer ${data.token}`);
        localStorage.setItem("username", data.username);
        history.push("/");
      }
    } catch (error) {
      console.log("Error in login");
    }
  };

  const signupHandler = () => {
    history.push('/signup');
  };
  return (
    <Flex height='100vh' alignItems="center" justifyContent="center" background="#faf0e6">
      <Flex height='400px' width='400px' direction="column" background="	#eed9c4" p={12} borderRadius={5}>

        <Heading mb={6} textAlign="center" color="#7D5A50">Log in</Heading>
        <Input color="#7D5A50" placeholder="email" variant="filled" mb={3} _placeholder={{ color: "white" }} type="email" background="#B4846C" name="email" id="email" onChange={changeHandler} />
        <Input color="#7D5A50" placeholder="password" variant="filled" mb={3} _placeholder={{ color: "white" }} type="password" background="#B4846C" name="password" id="password" onChange={changeHandler} />
        <Button alignSelf="center" width="50%" background="#faf8ea" color="#7D5A50" disabled={!email || !password} onClick={submitHandler}>Log in</Button>
        <Text alignSelf="center" color="red" fontWeight="bold" mt={2}>{error}</Text>

       
          <Flex alignItems="center" flexDirection="column" >
            <Text color="#7D5A50">Don't have an account yet? Create one! </Text>
            <Button width="fit-content" mt={2} background="#faf8ea" color="#7D5A50" onClick={signupHandler}>Sign up</Button>
          </Flex>
        
      </Flex>
    </Flex>
  );
}

export default Login;
