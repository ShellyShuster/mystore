const { async } = require("@firebase/util");
const { request } = require("express");
const firebaseConfig = require("../firebaseConfig");
const { db } = require("../util/admin");


const getProductById = async (productId) => {
  const product = await db.doc(`/products/${productId}`).get();
  if (product.exists) {
    return await product.data();
  }
};


exports.getUserCart = async (request, response) => {
  try {
    const data = await db
      .collection("carts")
      .where("user", "==", request.user.username)
      .limit(1)
      .get();

    const productsFromCart = data.docs[0].data().products;
    const products = [];
    for (let product of productsFromCart) {
      const p = await getProductById(product.id);
      if (p !== undefined) {
        const name = p.name
        const price = p.price;
        console.log(name, " + ", price)
        const item = {
          name: name,
          price: price,
          id: product.id,
          amount: product.amount
        }
        products.push(item);
      }
    }
    return response.status(200).json({ products, totalPrice: data.docs[0].data().totalPrice });
  } catch (e) {
    console.log("checkk");
    console.log(e);
  }
};

exports.addProductToUsersCart = async (request, response) => {
  const productToAdd = await db.doc(`/products/${request.body.id}`).get();
  const data = await db
    .collection("carts")
    .where("user", "==", request.user.username)
    .limit(1);

  const snapshot = await data.get();

  const cart = snapshot.docs[0].data();

  console.log("SEARCHING", cart);
  const index = cart.products.findIndex((item) => item.id === request.body.id);
  console.log(index);
  if (index !== -1) {
    console.log("");
    cart.products[index].amount += request.body.amount;
  } else {
    cart.products.push({ ...request.body });
  }
  console.log("id = " + request.body.id);
  console.log(productToAdd);
  cart.totalPrice += request.body.amount * productToAdd.data().price;
  const newData = await snapshot.docs[0].ref.update(cart);
  return response.status(200).json(newData);
};

exports.deleteProductFromCart = async (request, response) => {
  const productToDelete = await db.doc(`/products/${request.params.id}`).get();
  const data = await db
    .collection("carts")
    .where("user", "==", request.user.username)
    .limit(1);


  const snapshot = await data.get();
  const cart = snapshot.docs[0].data();
  const index = cart.products.findIndex((item) => item.id === request.params.id);
  let amount = 0;

  if (index !== -1) {
    cart.products[index].amount = cart.products[index].amount - 1;
  }

  cart.totalPrice -= amount * productToDelete.data().price;
  const newData = await snapshot.docs[0].ref.update(cart);
  return response.status(200).json(newData);
};

exports.clearCart = async (request, response) => {
  const data = await db
    .collection("carts")
    .where("user", "==", request.user.username)
    .limit(1);

  const snapshot = await data.get();
  const cart = snapshot.docs[0].data();
  cart.products = [];
  cart.totalPrice = 0;
  const newData = await snapshot.docs[0].ref.update(cart);
  return response.status(200).json(newData);
};
