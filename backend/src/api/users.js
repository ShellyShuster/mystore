const { admin, db } = require("../util/admin");
const config = require("../firebaseConfig");

const firebase = require("firebase/app");
firebase.initializeApp(config);
const {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
} = require("firebase/auth");
const auth = getAuth();

const { validateLoginData, validateSignUpData } = require("../util/validators");
const { response } = require("express");

// Login
exports.loginUser = async (request, response) => {
  const user = {
    email: request.body.email,
    password: request.body.password,
  };

  const { valid, errors } = validateLoginData(user);
  if (!valid) return response.status(400).json(errors);
  try {
    const data = await signInWithEmailAndPassword(
      auth,
      user.email,
      user.password
    );
    const token = await data.user.getIdToken();
    console.log("Login Succedded");
    response.json({ token });
  } catch {
    response.json({ error: "Wrong password!" });
  }

};

exports.signUpUser = async (request, response) => {
  const newUser = {
    email: request.body.email,
    password: request.body.password,
    confirmPassword: request.body.confirmPassword,
    username: request.body.username,
  };

  const { valid, errors } = validateSignUpData(newUser);

  if (!valid) return response.status(400).json(errors);

  let token, userId;
  let doc = await db.doc(`/users/${newUser.username}`).get();

  if (doc.exists) {
    return response
      .status(400)
      .json({ username: "this username is already taken" });
  } else {
    const userData = await createUserWithEmailAndPassword(
      auth,
      newUser.email,
      newUser.password
    );
    userId = userData.user.uid;
    const token = await userData.user.getIdToken();
    const userCredentials = {
      username: newUser.username,
      password: newUser.password,
      email: newUser.email,
      creationDate: new Date().toISOString(),
      userId,
    };
    await db.doc(`/users/${newUser.username}`).set(userCredentials);
    await initUserCart(newUser.username);
    return response.status(201).json({ token });
  }
};

const initUserCart = async (username) => {
  const newCart = {
    products: [],
    totalPrice: 0,
    user: db.doc("users/" + username),
  };
  await db.collection("carts").add(newCart);
};

exports.getUserDetail = async (request, response) => {
  let userData = {};
  const doc = await db.doc(`/users/${request.user.username}`).get();
  if (doc.exists) {
    userData.userCredentials = doc.data();
    return response.json(userData);
  }
};
