const { db } = require("../util/admin");


exports.getAllproducts = async (request, response) => {
  const data = await db
    .collection("products")
    .get();

  let products = [];
  data.forEach((doc) => {
    products.push({
      productId: doc.id,
      name: doc.data().name,
      creationDate: doc.data().creationDate,
      description: doc.data().description,
      owner: doc.data().owner,
      picUrl: doc.data().picture,
      price: doc.data().price,
      quantity: doc.data().quantity,
    });
  });

  console.log(products);
  return response.json(products);
};

exports.getOneProduct = async (request, response) => {
  const product = await db.doc(`/products/${request.params.productId}`).get();
  if (product.exists) {
    return response.json(product.data());
  }
};


exports.addProduct = async (request, response) => {
  const data = request.body.product;
  data.owner = request.user.username;
  const res = await db.collection('products').doc().set(data);

  return response.json({});
}


exports.deleteProduct = async (request, response) => {
  const res = await db.collection('products').doc(request.params.id).delete();

  return response.json({});
}

