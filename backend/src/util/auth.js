const { admin, db } = require('./admin');

module.exports = async (request, response, next) => {
    let idToken;
    console.log(request.headers);
    if (request.headers.authorization && request.headers.authorization.startsWith('Bearer ')) {
        idToken = request.headers.authorization.split('Bearer ')[1];
    } else {
        console.error('No token found');
        return response.status(403).json({ error: 'Unauthorized' });
    }
    try {
        const decodedToken = await admin
            .auth()
            .verifyIdToken(idToken)

        request.user = decodedToken;
        console.log(decodedToken)
        const data = await db.collection('users').where('userId', '==', request.user.uid).limit(1).get();

        request.user.username = data.docs[0].data().username;
        return next();
    }
    catch (e) {
        console.log("checkk in authhh");
        console.log(e.code);
        //throw e.code;
        //res.status(500).json({message: "Something went wrong, try again"})
        response.status(299).send(e.code);
    };
};