const { Router } = require("express");
const { deleteProductFromCart, clearCart } = require("../api/carts");
const { deleteProduct } = require("../api/products");
const ash = require("../ash");
const router = new Router();
const auth = require("../util/auth");

//carts
router.delete("/cart/products/:id", ash(auth), ash(deleteProductFromCart));
router.delete("/cart/", ash(auth), ash(clearCart));

//products
router.delete("/products/:id", ash(auth), ash(deleteProduct))

module.exports = router;
