const { Router } = require("express");
const { loginUser, signUpUser, signoutUser } = require("../api/users");
const { addProductToUsersCart } = require("../api/carts");
const ash = require("../ash");
const router = new Router();
const auth = require("../util/auth");
const { addProduct } = require("../api/products");

//users
router.post("/login", ash(loginUser));
router.post("/signup", ash(signUpUser));

//carts
router.post("/cart/products", ash(auth), ash(addProductToUsersCart));

//products
router.post('/products', ash(auth), ash(addProduct));

module.exports = router;
