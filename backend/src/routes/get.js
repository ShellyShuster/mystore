const { Router } = require("express");
const products = require("../api/products");
const carts = require("../api/carts");
const { getUserDetail } = require("../api/users");
const ash = require("../ash");
const auth = require("../util/auth");
const { app } = require("firebase-admin");

const router = new Router();

//users
router.get("/user", ash(auth), ash(getUserDetail));

//products
router.get("/products", ash(products.getAllproducts));
router.get("/product/:productId", ash(products.getOneProduct));

//carts
router.get("/cart", ash(auth), ash(carts.getUserCart));

module.exports = router;
