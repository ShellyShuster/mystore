const getRouter = require("./routes/get");
const postRouter = require("./routes/post");
const deleteRouter = require("./routes/delete");
const cors = require('cors');

//Express
const express = require("express");

//body-parser
const bodyParser = require("body-parser");
//using express
const app = express();


// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
//   next();
// })

app.use(cors({
  origin: 'http://localhost:3001'
}));

//using bodyparser
app.use(bodyParser.json());

app.use(express.static("static"));

app.use(postRouter);
app.use(getRouter);
app.use(deleteRouter);

app.listen(3000, () => {
  console.log("server start");
});
